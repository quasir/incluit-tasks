/**
 * `arrLeft` consists of a function to rotate an array of integers to the left the specified number of times
 *
 * @param  {Array} `arr`
 * @param  {Integer} `number`
 */
export let arrLeft = (arr, number) => {
  let memoization;
  for (let index = 0; index < number; index++) {
    for (let i = 0; i < number; i++) {
      if (arr.length < number) return arr;
      //pure way to rotate an array
      memoization = arr[i];
      arr[i] = arr[i + 1];
      arr[i + 1] = memoization;
    }
  }

  return arr;
};


