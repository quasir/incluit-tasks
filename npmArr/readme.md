# NPM profile: visit this package in npm


https://www.npmjs.com/package/arrayleft


# Use Example
```bash
import { arrLeft } from "./index.js";

let arr = [1, 2, 3, 4, 5];

let arrToLeft = arrLeft(arr, 4);

console.log(arrToLeft);

//Output : [5, 1, 2, 3, 4];
```
