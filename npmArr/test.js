import { arrLeft } from "./index.js";
let bool = false;
let arr = [1, 2, 3, 4, 5];

let arrToLeft = arrLeft(arr, 4);

let arrExpected = [5, 1, 2, 3, 4];

for (let i = 0; i < arrToLeft.length; i++) {
  if (arrToLeft.length !== arrExpected.length) {
    throw new Error(`${arrToLeft} es distinto de ${arrExpected}`);
  }
  if (arrToLeft[i] == arrExpected[i]) {
    bool = true;
  } else {
    throw new Error(`${arrToLeft} es distinto de ${arrExpected}`);
  }
}

if (bool) console.log("Success");
