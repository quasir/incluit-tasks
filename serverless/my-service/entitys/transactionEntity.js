//Transaction schema
module.exports.transactionEntity ={
    type: "object",
    properties:{
        transactions:{
            type:"object",
            strict:true,
            properties:{
                amount: {type: "number",gt:0}, // Pick a positive number
                currency: {type:"string",eq:["ars","usd"]}, // ARS or USD
                vendor: {type: "string"},   // Pick a random name
                status: {type: "string",eq:["succeeded","fail"] }   // Allowed: "succeeded" or "fail"
            }
        }
    }     
};