const inspector = require("schema-inspector");
const { transactionSchema } = require("./entitys/transactionEntity");

/**
 * A function used to iterate on each item from the
 * array and calculate the total balance in different currencies
 * @param  {Array} transactions
 * @return {Object}
 */
function calculateBalance(transactions) {
  let balanceInARS = 0;
  let balanceInUSD = 0;

  for (const transaction in transactions) {
    const { currency, status, amount } = transactions[transaction];
    if (status === "succeeded") {
      currency === "ars"
      ? (balanceInARS += amount)
      : (balanceInUSD += amount);
    }
  }

  return {
    amounts: {
      ars: balanceInARS,
      usd: balanceInUSD,
    },
  };
}

module.exports.main = (event, context, callback) => {
  const payload = JSON.parse(event.body);
  const { transactions } = payload;
  const result = inspector.validate(transactionSchema, payload);

  // Reject if the result of validation fails
  if (!result.valid) {
    callback(null, {
      statusCode: 400,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        message: "Unable to process the request due to invalid syntax",
      }),
    });
  }

  callback(null, {
    statusCode: 200,
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      balance: calculateBalance(transactions),
    }),
  });
};
