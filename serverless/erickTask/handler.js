'use strict';



module.exports.hello = async (event,req,res) => {
  const {message} = req.body;
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Hello World',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
module.exports.bye = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Bye World',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
