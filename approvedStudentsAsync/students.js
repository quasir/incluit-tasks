// Your code here!
var fs = require("mz/fs");

//Funcion para Capitalizar texto 
function capitalizeFirstLetter(string) {
  string = string.toLowerCase();
  return string.charAt(0).toUpperCase() + string.slice(1);
}

//Funcion para leer el JSON
async function readStudentsJson() {
  try {
    //Traer y esperar la data de students.json ()
    const data = await fs.readFile('./students.json');
    //Parsear la data y asignarla a una variable OBJ
    const students = JSON.parse(data);

    //Retornar los estudiantes
    return students;
  } catch (error) {
    console.log(error);
  }
}

//Funcion para saber quienes aprobaron y ordenaros de modo ascendente dependiendo del argumento que se le pase
async function approved(qualification) {
  const students = await readStudentsJson();

  students.map((e) => {
    if (e.score > qualification) {
      let name, lastname;
      name = capitalizeFirstLetter(e.name);
      console.log(` ${name} aprobo con una nota de: ${e.score}`);
    }
  });

}

approved(2);
