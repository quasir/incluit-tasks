// Your code here!
let students = [
    {
      name: 'JOHN',
      lastname: 'DOE',
      score: 4
    },
    {
      name: 'EVELYN',
      lastname: 'JACKSON',
      score: 8
    },
    {
      name: 'JAMES',
      lastname: 'SHAW',
      score: 2
    }
  ];
  
  function capitalizeFirstLetter(string) {
      string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  
  function aprobados(arg){
  
    students.map((e)=>{
      if(e.score > arg){
          let name, lastname;
          name = capitalizeFirstLetter(e.name);
          console.log(` ${name} aprobo con una nota de: ${e.score}`)
      }
    });
  }
  
  aprobados(2);