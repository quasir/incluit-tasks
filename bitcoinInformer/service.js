import { createRequire } from "module";
const require = createRequire(import.meta.url);

const axios = require("axios").default;

export let getDolarOfficialPrice = async () => {
  try {
    const { data } = await axios.get(
      "https://www.dolarsi.com/api/api.php?type=valoresprincipales"
    );
    const dolarOfficial = data
      .filter((element) => element.casa.nombre == "Dolar Oficial")
      .map((element) => element.casa.compra);

    return parseFloat(dolarOfficial);
  } catch (error) {
    console.log(error);
  }
};

export let getBitcoinDolarPrice = async () => {
  try {
    const { data } = await axios.get(
      "https://api.coindesk.com/v1/bpi/currentprice.json"
    );
    const bitcoinDolarPrice = data.bpi.USD.rate_float;
    return parseFloat(bitcoinDolarPrice);
  } catch (error) {
    console.log(error);
  }
};
