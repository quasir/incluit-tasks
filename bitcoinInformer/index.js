import { getDolarOfficialPrice,getBitcoinDolarPrice } from "./service.js";

let getBitcoinPrice = async () => {
  let dolarOfficialPrice = await getDolarOfficialPrice();
  let bitcoinDolarPrice = await getBitcoinDolarPrice();
  
  console.log(`El precio del bitcoin en pesos argentinos es : ${dolarOfficialPrice * bitcoinDolarPrice} `);
}

getBitcoinPrice();
