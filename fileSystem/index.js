const { readFileSync, appendFileSync } = require("fs");

const main = (function () {
  try {
    const data = readFileSync("./cashflow.txt");
    //Convierto la data en string
    let stringfile = data.toString().split("\n"); //Convierto en un array separando por saltos de linea
    stringfile.shift(); //Elimino el primer elemento
    let total_amount = 0;

    stringfile.forEach((detail) => {
      //Convierto en entero la data de los substrings a partir del indice 15
      const amount = parseInt(detail.substring(15).trim());
      if (amount) total_amount += parseInt(amount);
    });
    //Agregar el monto al final del documento
    appendFileSync("./cashflow.txt", total_amount.toString());
    console.log("The total was appended succesful");
  } catch (e) {
    console.log(e);
  }
})();
